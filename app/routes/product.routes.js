var product  = require("../controllers/product.controller");

module.exports = function(app){
    app.get('/cart', product.showCart);
    app.post('/addCart',product.addCart);
    app.get('/insert', product.insertItem);
    app.get('/find',product.findAllItem);
    app.get('/remove',product.removeItem);
    app.post('/detail',product.findItem);
    app.get('/detail',product.findDetail);
    app.get('/insertDetail',product.insertDetail);
    app.get('/brand/:name',product.findBrand);
    app.get('/search',product.searchItem);
    app.get('/insertBrand',product.insertBrand);
    app.get('/findBrandDetail', product.findBrandDetail);
  
}
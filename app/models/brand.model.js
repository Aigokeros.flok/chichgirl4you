var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Brands = new Schema({
	brands :{
		type:String,
		unique:true
	},  
    detail:String,
	website:String,
	office:String,
	contract:String,
    address:{type:String}
});

	
	mongoose.model('Brands',Brands);
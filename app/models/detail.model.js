var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Detail = new Schema({
	id :{
		type:String,
		unique:true
	},  
    describe:{
		type:String
	},
	subDetail:{type:Array},
	usage:{type:Array},
	subUsage:{type:Array},
    intredients:{type:Array}
});

	
	mongoose.model('Detail',Detail);
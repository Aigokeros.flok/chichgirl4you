User = require('mongoose').model('User');

exports.saveOAuthUserProfile = function(req, profile, done){
    User.findOne({
        provider: profile.provider,
        providerId: profile.providerId
    }, function(err, user){
        if(err) return done(err);
        else{
            if(!user){
                var possibleUsername = profile.username
                    || (profile.email ? profile.email.split('@')[0] : '');
                User.findUniqueUsername(possibleUsername, null, function(avalibleUsername){
                    profile.username = avalibleUsername;
                    user = new User(profile);
                    user.save(function(err){
                        if(err){
                            var message = getErrorMessage(err);
                            req.flash('error', message);
                            return req.redirect('/',{login_message: 'login fail'});
                        }
                        return done(err,user);
                    })
                })

            }else{
                return done(err, user);
            }
        }
    });

}


var getErrorMessage = function(err){
	var message ='';
	
	if(err.code){
		switch(err.code){
			case 11000:
			case 11001:
				message = 'Username already exitsts';
				break;
			default:
				message = 'Something went wrong';
		}
	}else{
		for(var errName in err.errors){
			if(err.errors[errName].message){
			message = err.errors[errName].message;
			}
		}
	}
	return message;
}

exports.Renderlogin = function(req, res){
   /* if(typeof req.session.isLogin !== 'undefined'){
        isLogin = req.session.isLogin;
        console.log('define session');
    }*/
        res.render('test', {isLogin:req.session.isLogin, user:req.session.user});
}

exports.login = function(req, res){
        req.session.user = req.body.name;
        req.session.month = req.body.month;
        req.session.isLogin = true;
        console.log(req.session.user);
        res.redirect('/');
}

exports.logout = function(req,res,next){
    req.session.destroy(function(err) {
        if(err){
            console.log("cann't destroy session");
        }
     })
        res.redirect('/');
}


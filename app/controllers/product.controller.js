var xlsx = require('node-xlsx');
var Product = require("mongoose").model("Product");
var Detail = require("mongoose").model("Detail");
var Brands = require("mongoose").model("Brands");
var object_product = xlsx.parse(__dirname + '/../../public/xls/product.xlsx');
var object_comment = xlsx.parse(__dirname + '/../../public/xls/comment.xlsx');
var object_brands =  xlsx.parse(__dirname + '/../../public/xls/brands.xlsx');
var history = [];
var cart =[];
//var object_comment = xlsx.parse(__dirname + '/../../public/xls/list_detail/comment.xlsx');   
const fs = require('fs');

var pic_collection =[];
var detail_collection =[];
var usage_collection =[];
var review_collection =[];
var in_collection =[];

exports.removeItem = function(req, res){
	Product.remove(function(err){
		if(err){
				console.log("cann't remove file");
		}else{
			console.log("remove success...");
		}
	});

}

exports.insertDetail = function(req, res){
	Detail.remove(function(err){
		if(err){
			console.log("cann't remove file");
		}else{
			console.log("remove Detail success...");
		}
	});
	//--------------------------------------------------------
	var x = {};
	var y = [];
	var targetURI2 = __dirname + '/../../public/xls/list_detail';
	fs.readdirSync(targetURI2).forEach(file => {
	var obj_detail = xlsx.parse(__dirname + '/../../public/xls/list_detail/'+file); 
		var detail = new Detail();	
		detail.id = obj_detail[0].data[1][0];
		detail.describe = obj_detail[0].data[1][1];
		for(var i=1; i<=obj_detail[0].data[1][6]; i++){
			detail.subDetail.push(obj_detail[0].data[i][2]);
		}
		for(var i=1; i<=obj_detail[0].data[1][7]; i++){
			detail.usage.push(obj_detail[0].data[i][3]);
		}
		for(var i=1; i<=obj_detail[0].data[1][8]; i++){
			detail.subUsage.push(obj_detail[0].data[i][4]);
		}
		for(var i=1; i<=obj_detail[0].data[1][9]; i++){
			detail.intredients.push(obj_detail[0].data[i][5]);
		}
		console.log(detail);
		
		detail.save(function(err){
			if(err){
				console.log("err : " + err);
			}else{
				console.log("success....");
				
			}
		});
	});
	
}

exports.insertBrand = function(req, res,next){
	
	Brands.remove(function(err){
		if(err){
			console.log("cann't remove file");
		}else{
			console.log("remove Brands success...");
		}
	})
	
	console.log( object_brands[0].data.length);
	for(var i=1; i<=object_brands[0].data.length;i++){
		var brand = new Brands();
		brand.brands = object_brands[0].data[i][0];
		brand.detail = object_brands[0].data[i][1];
		brand.website = object_brands[0].data[i][2];
		brand.office = object_brands[0].data[i][3];
		brand.contract = object_brands[0].data[i][4];
		brand.address = object_brands[0].data[i][5];
		brand.save(function(err){});
		console.log(brand);
	}
	
	
}

exports.findBrandDetail = function(req,res,next){
	Brands.find({}, function(err, data){
		if(err){
			next(err);
		}else{
			res.json(data);
		}
	})
}

exports.insertItem = function(req, res){

//----------------------------------------------
	for(var i=0; i< object_product[0].data.length; i++){
		var item = new Product();
		item.id = object_product[0].data[i][0];
		item.brand = object_product[0].data[i][1];
		item.name = object_product[0].data[i][2];
		item.size = object_product[0].data[i][3];
		item.price = object_product[0].data[i][4];
		item.sale = object_product[0].data[i][5];
		item.saveMoney = object_product[0].data[i][6];
		item.shortDetail = object_product[0].data[i][7];
		var rand = getRandomInt(9)+3;
		var text = {};
		for(var j=1; j<rand; j++){
			var author = getRandomInt(20)+1;
			text.author = object_comment[0].data[author][1];
			text.text = object_comment[0].data[getRandomInt(20)+1][0];
			text.rating = object_comment[0].data[author][2];
			text.date = object_comment[0].data[getRandomInt(20)+1][3];
			text.like = object_comment[0].data[getRandomInt(20)+1][4];
			item.comment.push(text);
		}
	
		item.save(function(err){})
	}
	console.log("success... length = " + object_product.data.length);
//---------------------------------------------

}

function getRandomInt(max) {
	return Math.floor(Math.random() * Math.floor(max));
}
  

exports.findAllItem = function(req,res){
	Product.find({},function(err,data){
		if(err){
			console.log(err);
		}else{
			res.json(data);
		}
	})
}

exports.findDetail = function(req,res){
	Detail.find({}, function(err,data){
		if(err){
			console.log(err);
		}else{
			res.json(data);
		}
	})
}


exports.findItem = function(req,res){

	pic_collection = [];
	
	detail_collection =[];
	usage_collection =[];
	review_collection =[];
	in_collection =[];
	var have_vid  = false;
	var empty = {};
	var empty2 = {};
	var eid = req.body.id;
	if(req.session.history !=null){
		history = req.session.history;
		history = JSON.parse(history);
		console.log("history!=null");
	}else{
		history = [];
		console.log("history==null")
	}

	//-------^^^ new ^^^-----------------
	var amount = 0;
	if(req.session.cart!=null){
		cart = req.session.cart;
		cart = JSON.parse(cart);
		for(var i=0 ; i< cart.length ; i++){
			amount+= cart[i].amount;
		}
	}else{
		amount = 0;
		cart = [];
	}
	//-----------------------------------

	Product.findOne({id:eid}, function(err,data){
			if(err){
				console.log("Your error : " +err);
			}else{
				console.log('i will send data');
				//--------- find main pic collection ----------------
				var targetURI = './public/images/product/'+data.brand+'/'+data.name+'/product_collection'	
				fs.readdirSync(targetURI).forEach(file => {
					pic_collection.push(file);
				});

				var targetURI = './public/images/product/'+data.brand+'/'+data.name+'/brand'
				fs.readdirSync(targetURI).forEach(file => {
					detail_collection.push(file);
				});
				console.log("detail file : "+ detail_collection.length);
				var targetURI = './public/images/product/'+data.brand+'/'+data.name+'/usage'
				fs.readdirSync(targetURI).forEach(file => {
					usage_collection.push(file);
				});

				var targetURI = './public/images/product/'+data.brand+'/'+data.name+'/review'
				fs.readdirSync(targetURI).forEach(file => {
					review_collection.push(file);
				});

				var targetURI = './public/images/product/'+data.brand+'/'+data.name+'/ingredient'
				fs.readdirSync(targetURI).forEach(file => {
					in_collection.push(file);
				});

				var targetURI = './public/images/product/'+data.brand+'/'+data.name+'/video'
				fs.readdirSync(targetURI).forEach(file => {
					if(file!=null){
						have_vid = true;
					}
				});
					
				 empty = {
					id:data.id, 
					brand:data.brand, 
					name: data.name,
					size:data.size, price: 
					data.price, 
					sale: data.sale, 
					saveMoney: data.saveMoney, 
					shortDetail: data.shortDetail,
					rating: data.rating,
					comment:data.comment,
					pic_col:pic_collection};
					history.unshift(empty);
			
					//console.log("after push empty to history : " + history);

					req.session.history = JSON.stringify(history);
					console.log(req.session.history);
					//console.log("I'm empty id: "+empty.rating);

					Detail.findOne({id:eid},function(err,data){
						if(err){
							console.log("Your error : " +err);
						}else{

							empty2 = {
								id:empty.id, 
								brand:empty.brand, 
								name: empty.name,
								size:empty.size, 
								price: empty.price, 
								sale: empty.sale, 
								saveMoney: empty.saveMoney, 
								shortDetail: empty.shortDetail,
								rating: empty.rating,
								comment:empty.comment,
								pic_col:pic_collection,
								detail:data.describe,
								subDetail:data.subDetail,
								usage:data.usage,
								subUsage:data.subUsage,
								intredients:data.intredients
								};

							Brands.findOne({brands:empty2.brand}, function(err, data){
								if(err){
									next(err);
								}else{
									console.log(amount);
									//console.log("brand id "+ eid +" : " + data);
									res.json({
										id:empty2.id, 
										brand:empty2.brand, 
										name: empty2.name,
										size:empty2.size, 
										price: empty2.price, //use
										sale: empty2.sale, //use
										saveMoney: empty2.saveMoney, //use
										shortDetail: empty2.shortDetail, //use
										rating: empty2.rating, //use
										comment:empty2.comment, //use
										pic_col:pic_collection, //use
										detail:empty2.detail, //use
										subDetail:empty2.subDetail, //use
										usage:empty2.usage, //use
										subUsage:empty2.subUsage, //use
										intredients:empty2.intredients,
										brand_name: data.brand, //use
										brand_detail:data.detail, //use
										brand_website:data.website, //use
										brand_office:data.office, //use
										brand_contract:data.contract, //use
										brand_address:data.address, //use
										detail_col: detail_collection,
										usage_col:usage_collection,
										review_col:review_collection,
										in_col:in_collection,
										history:history,
										have_vid:have_vid,
										cart_amount:amount // ^^^new
									});
								}
							})
													//console.log(data);
							
						}
					});
			}
	});

}

exports.findBrand = function(req, res, next){
	console.log(req.params.name);
	Product.find({brand:req.params.name}, function(err, data){
		if(err){
			console.log("Cann't find brand : "+err);
			next(err);
		}else{
			res.json(data);
		}
	});
}

exports.showCart = function(req, res){

	if(req.session.history !=null){
		history = req.session.history;
		history = JSON.parse(history);
		console.log("history!=null");
	}else{
		history = [];
		console.log("history==null")
	}
	var amount = 0;
	if(req.session.cart!=null){
		cart = req.session.cart;
		cart = JSON.parse(cart);
		for(var i=0 ; i< cart.length ; i++){
			amount+= cart[i].amount;
		}
	}else{
		amount = 0;
		cart = [];
	}
	cart.cart_amount = amount;
	var empty = {};
	var arr = [];
	Detail.find({}, function(err,data){
			for(var i =0; i<cart.length; i++){
				for(var j=0; j< data.length; j++){
					if(cart[i].id==data[j].id){
						empty = {
							id : cart[i].id,
							brand : cart[i].brand,
							price : cart[i].price,
							sale : cart[i].sale,
							saveMoney: cart[i].saveMoney,
							shortDetail: cart[i].shortDetail,
							name : cart[i].name,
							detail : data[j].describe,
							amount: cart[i].amount,
							rating: cart[i].rating
						}
						arr.unshift(empty);
					}
				}
			}
			console.log(arr);
			arr.cart_amount = amount;
			arr.history = history;
		res.render("cart.ejs",{Item:arr});
	})

	
	
}

exports.addCart = function(req,res){
	var empty = {};
	if(req.session.cart!=null){
		console.log("cart != null");
		cart = req.session.cart;
		cart = JSON.parse(cart);
	}else{
		console.log("cart == null");
		cart = [];
	}
	if(cart.map(x => x.id).includes(req.body.id)){
		cart.find(x=>x.id==req.body.id).amount = cart.find(x=>x.id==req.body.id).amount+1;
		req.session.cart = JSON.stringify(cart);
		//res.json({haveItem:cart.find(x=>x.id==req.body.id).amount});
		res.json(cart);
	}else{
		Product.findOne({id:req.body.id}, function(err,data){
			if(err){
				console.log("err");
			}else{

				empty = {
					id:data.id, 
					brand:data.brand, 
					name: data.name,
					size:data.size, 
					price:data.price, 
					sale: data.sale, 
					saveMoney: data.saveMoney, 
					shortDetail: data.shortDetail,
					rating: data.rating,
					comment:data.comment,
					amount:1};
				
				cart.unshift(empty);
				req.session.cart = JSON.stringify(cart);
				console.log("cart : "+ req.session.cart);
				res.json(cart);
			}
			
		});
	}
		
}

exports.searchItem = function(req, res, next){
	var x = "/"+req.query.text+"/";
	var result_product = {};
	Product.find({brand:{ $regex: req.query.text, $options: 'i' }},function(err,data){
		
		if(err){
			console.log(err);
		}else{
			result_product=data;
			var arr_check = result_product.map(x=>x.id);
			Product.find({name:{ $regex: req.query.text, $options: 'i' }},function(err,data){
					for(var i=0; i<data.length; i++){
						
						if(!arr_check.includes(data[i].id)){
							result_product.push(data[i]);
						}
					}
					arr_check = result_product.map(x=>x.id);
					Product.find({shortDetail:{ $regex: req.query.text, $options: 'i' }},function(err,data){
						for(var i=0; i<data.length; i++){
							if(!arr_check.includes(data[i].id)){
								result_product.push(data[i]);
							}
						}
					});
					res.json(result_product);
			}); // end find by name
		}
	}); //end find by brand
}
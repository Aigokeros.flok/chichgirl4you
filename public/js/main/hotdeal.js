

$("div.hot_deal > .content > ul > li").click(function(){
	var key = $(this).eq(0)[0].children[0].innerHTML;
	localStorage.setItem('p_id',key);
	window.open('/product', '_self');
})

$("div.bestSeller > .content").click(function(){
	
	localStorage.setItem('p_id',4);
	window.open('/product', '_self');
})

var time_hotdeal = setInterval(updateTime,1000);

function updateTime(){
	var day = $('div.hot_deal .count_time > div').eq(0)[0].children[1].innerHTML;
	var hour = $('div.hot_deal .count_time > div').eq(1)[0].children[1].innerHTML;
	var min = $('div.hot_deal .count_time > div').eq(2)[0].children[1].innerHTML;
	var sec = $('div.hot_deal .count_time > div').eq(3)[0].children[1].innerHTML;
	sec--;
	if(sec<0){
		sec = 60
		min--;
	}
	if(min<0){
		min = 60;
		hour--;
	}
	if(hour<0){
		hour = 24;
		day--;
	}
	if(day<0){
		clearInterval(time_hotdeal);
	}

	$('div.hot_deal .count_time > div').eq(0)[0].children[1].innerHTML = day;
	$('div.hot_deal .count_time > div').eq(1)[0].children[1].innerHTML = hour;
	$('div.hot_deal .count_time > div').eq(2)[0].children[1].innerHTML = min;
	$('div.hot_deal .count_time > div').eq(3)[0].children[1].innerHTML = sec;

}
var grobal_data;
//----------------------------- history ---------------------------------
var h_p = 0;
function scroll_history(vector){
   // var width = $('.product_history > .history > ul')[0].scrollWidth;
   var width = ($('.history > ul > li').length-4) * $('.history > ul > li').outerWidth(true); // -4 คือเราโชว์ 4 รูป เราเลื่อนอีก 6 รูป
    if(vector>0){
        h_p += $('.history > ul > li').outerWidth(true);
    }else{
        h_p -= $('.history > ul > li').outerWidth(true);
    }

    if(h_p > width){
        h_p = width;
        $('#test').html('in here');
    }else if(h_p < 0){
        h_p = 0;
    }
    $('.product_history > .history > ul').animate({scrollLeft:h_p});
    $('#test').html(h_p);
}




//-----------------------------end history ---------------------------------
var s_c = 0;
function scroll_product(vector){
    var height = ($("div.right_in_left > ul > li").length-3) *  $("div.right_in_left > ul > li").outerHeight(true) ;
    
    if(vector>0){
        console.log('up');
        s_c -= $("div.right_in_left > ul > li").outerHeight(true);
    }else{
        console.log('down');
        s_c += $("div.right_in_left > ul > li").outerHeight(true);
    }
    
    if(s_c > height){
        s_c = height;
    }else if(s_c <= 0){
        s_c = 0;
    }
    console.log(s_c);
  //  console.log($("div.right_in_left > ul > li").eq(0).offset().top);
  //  console.log($("div.right_in_left > ul > li").eq(1).offset().top);
    $("div.right_in_left > ul").animate({scrollTop:s_c});

  //  $("div.right_in_left > ul").scrollTop(s_c);
    
}
//----------------------------- product detail -----------------------------

$(document).ready(function(){
  
    var mes;
    var pic_col = "<ul>";
    $.ajax({
        url : "/detail",
        type : "POST",
        data : {
            id : localStorage.getItem('p_id')
        },
        success : function(data) {
          grobal_data = data;
          // document.getElementById("navi").innerHTML = "<a href = '/'> Home </a> <span> > </span> <a href = '/'>" + data.brand +  "</a> <span> > </span> <a href = '/'>" + data.name +  "</a> "; 
           document.getElementById("navi").innerHTML = "Home > " +data.name;
            console.log(data.pic_col);
            //Home > Witty Merry > บำรุงผิว > Witty Merry Orchid Hand & Nail Cream 75g
            for(var i=0; i < data.pic_col.length;i++){
                pic_col += `<li>
                                <img src = "images/product/`+data.brand+`/`+data.name+`/product_collection/` + data.pic_col[i]+`">
                                </img>
                            </li>`;
            }   
            pic_col += `</ul> <div class = "up" onclick = "scroll_product(1)">UP</div>
            <div class = "down" onclick = "scroll_product(-1)">DOWN</div>`;
            
            mes += `<div class = "left clearfix">
                            <div class = "left_in_left">
                                <img src = "images/product/`+data.brand+`/`+data.name+`/product_collection/` + data.pic_col[0]+`">
                                <div class = "contain_rating">
                                    <img src = "images/rating/rating`+data.rating+`.png"/>
                                    <span class = "avg_rating"> `+data.rating+` </span>
                                </div>
                               <!-- <div class = "product_amount"> <div class = "fill_amount">จำนวน </div> </div> -->
                            </div>
                            <div class = "right_in_left">
                               
                            </div>
                     </div>
        <div class = "right">
            <p>`+ data.name +`</p>
            <div class = "color" > </div>
            <p class ="detail_product">`+ data.shortDetail + `</p>
            <!--<div class = "review_audio"><a class = "audio">รีวิวเสียงจากผู้ใช้</a><img src = "images/audio.png"> </div> -->
            <p> ราคา </p>
            <p class = "price"> </p> <span class = "sell">฿`+data.price+` </span>
            <div class = "basket"> หยิบใส่ตระกร้า </div>
        </div>`;

        var mes_vid = `<p class = "top clearfix">
                            <span class = "hide_vid">_</span>   
                        </p>
                        <video style ="width:100%;height:222px;" id="videoclip" controls="controls" poster="images/product/`+data.brand+`/`+data.name+`/brand/Brands.jpg" title="Video title">
                         <source id="mp4video" src="images/product/`+data.brand+`/`+data.name+`/video/1.mp4" type="video/mp4"/>
                        </video>`;
                     

        $('.product').html(mes);
        $('.video_content').html(mes_vid);

        

        if(!data.have_vid){
            $('.video_content').hide();
        }else{
            $('.video_content').show();
            var videocontainer = document.getElementById('videoclip');
            videocontainer.play();
            
        }

        if(data.sale!=0){
            $('.right > p.price').text("฿"+data.sale) ;
        }
        $('.right_in_left').html(pic_col);
        $("div.right_bar > div.new_wrapper_bar > ul > li > div.count_cart").html(data.cart_amount);
        $('.header_middle > div > .right .cart > a').html(data.cart_amount);
        setHistory(data);
        setDetail(data);
        setComment(data);
        setBasket(data);
        },error: function (jqXHR, exception) {
            console.log('your error here.');
            console.log(jqXHR);
            // Your error handling logic here..
        }
    });
    
    $(document).ajaxStop(function(){
        selectItem();
        $("#back_2_top").css("display","none");
        $('div.product_history > div.history > ul > li').click(function(){
            localStorage.setItem('p_id',$(this).eq(0)[0].children[0].innerHTML);
            window.open('/product', '_self');
        })
      // ------------------------------- set video hide-----------------------------------
      
      var vid_is_hidden = false;
        $('.video_content > .top > span.hide_vid').click(function(){
            if(vid_is_hidden){
                $(".video_content").css("height","250px");
                $(".video_content").css("width","350px");
                $("#back_2_top").css("display","none");
                $(this).html("_");
                vid_is_hidden = false;
            }else{
                $(".video_content").css("height","30px");
                $(".video_content").css("width","150px");
                $("#back_2_top").css("display","block");
                $(this).html("^");
                vid_is_hidden = true;
            }
        
        })
        //----------------------------- end video hide -------------------------------
  
    });   
});

function selectItem(){
    var imgLength = $("div.left > .right_in_left").eq(0).children(0).children(0).length;
   // console.log( $("div.left > .right_in_left").eq(0)[0].children[0].children[0]);
    //console.log($("div.left > .right_in_left").eq(0).children(0).children(0)); //() for object element
    $("div.left > .right_in_left > ul >li").eq(0).children(0).css("box-shadow","0 5px 10px rgba(0,0,0,0.15)");
    $("div.left > .right_in_left").eq(0).children(0).children(0).children(0).click(function(){
        for(var i=0; i< imgLength ;i++){
            $(this).parent().parent().children(0).children(0).eq(i).css("box-shadow","none");
        }
        $(this).css("box-shadow","0 5px 10px rgba(0,0,0,0.15)");
        $(this).css("transition","box-shadow 0.2s ease");
        $("div.left > .left_in_left > img").attr("src",$(this).attr("src"));
      
      //  console.log($(this)[0]);
    }); //end shadow and change pic
   
    // $("div.left > .right_in_left > ul").mouseover(function(){
       
    //     $(this).parent().find("div").css("display","block");
    //     $(this).parent().find("div").animate({opacity: 1});
        
    // });
   
    /*$("div.left > .right_in_left > div").mouseout(function(){
        $("div.left > .right_in_left > ul").mouseout(function(){
       
             $(this).parent().find("div").hide();
             $(this).parent().find("div").css("opacity","0");
             
     
         });
    })*/
   

}
//--------------------------------------------------------------------------
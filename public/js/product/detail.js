function setDetail(data){
    

    var mes = `<div class = "brand_line"> <img style = "width:100%;" src="images/decoration/brand.png"/> 
                    <div class = "brand"> 
                        <img class = "image_center"  src="images/product/`+data.brand+`/`+data.name+`/Brand/`+data.detail_col[0]+`"/> 
                    </div>
                    <div class = "brand_detail space_detail">
                       
                    </div> 
                </div>
                <div class = "detail_line"> <img style = "width:100%;" src="images/decoration/detail.png"/> 
                    <div class="product_detail space_detail">
                    </div>
                </div>
                <div class = "usage_line"> <img style = "width:100%;" src="images/decoration/usage.png"/> 
                    <div class="usage space_detail">
                    </div>
                </div>
                <div class = "In_line"> <img style = "width:100%;" src="images/decoration/in.png"/> 
                    <div class="in space_detail">
                    </div>
                </div>
                <div class = "review_line"> <img style = "width:100%;" src="images/decoration/review.png"/> 
                    <div class="review space_detail">
                    </div>
                </div>`;
            //------------------ brand detail ---------------------------
                var brand_detail =  "";
                if(data.bran_name != null){
                    brand_detail += `<p>`+ data.brand_name +`</p>`;
                }
                if(data.brand_detail != null){
                    brand_detail += `<p>`+ data.brand_detail+`</p>`;
                }
                if(data.brand_website!=null){
                    brand_detail+=`<p>`+data.brand_website+`</p>`
                }
                if(data.brand_office!=null){
                    brand_detail+=`<p>`+data.brand_office+`</p>`
                }
                if(data.brand_contract!=null){
                    brand_detail+=`<p>`+data.brand_contract+`</p>`
                }
                if(data.brand_address!=null){
                    brand_detail+=`<p>`+data.brand_address+`</p>`
                }
            //----------------------------------------------------------
            //------------------------ Detail ---------------------------
                var product_detail = "";
                if(data.detail != null){
                    product_detail += `<p class = "text_detail">`+data.detail+`</p>`;
                }
                for(var i=1; i<data.detail_col.length; i++){
                    product_detail += `<li style = "list-style:none;">
                                          <img class = "image_center" src = "images/product/`+data.brand+`/`+data.name+`/brand/`+data.detail_col[i]+`"/>
                                       </li>`
                }
                if(data.subDetail.lenght > 0){
                    for(var i=0; i<data.subDetail.length;i++){
                    product_detail += `<li class = "text_subDetail">`+data.subDetail[i]+`</li>`;
                    }
                }
            //----------------------------------------------------------
            //-------------------------- Usage -------------------------
                var usage = "";
                for(var i=0; i<data.usage_col.length;i++){
                    usage += `<li style = "list-style:none;">
                                <img class = "image_center" src = "images/product/`+data.brand+`/`+data.name+`/usage/`+data.usage_col[i]+`"/>
                              </li>`
                }

                if(data.usage.length>0){
                    usage += `<p class = "text_usage">`+ data.usage[0]+`</p>`
                }
                for(var i=0; i<data.subUsage.length;i++){
                    usage += `<li class = "text_subUsage">`+ data.subUsage[i] + `</li>`;
                }
            //----------------------------------------------------------
            //------------------------ In ------------------------------
                var intredients = "";
                for(var i=0;i< data.in_col.length;i++){
                    intredients += `<li style = "list-style:none;">
                                     <img class = "image_center" src = "images/product/`+data.brand+`/`+data.name+`/ingredient/`+data.in_col[i]+`"/>
                                    </li>`
                }
                for(var i=0; i< data.intredients.length;i++){
                    intredients += `<p class = "text_in">`+data.intredients[i]+`</p>`;
                }
                
            //----------------------------------------------------------
            //------------------------ review --------------------------
                var review = "";
                for(var i=0;i< data.review_col.length;i++){
                    review += `<li style = "list-style:none;">
                                    <img  class = "image_center" src = "images/product/`+data.brand+`/`+data.name+`/review/`+data.review_col[i]+`"/>
                                </li>`
                }
            //----------------------------------------------------------
        $('.content > div.content_detail').html(mes);
        $('.content > div.content_detail > .brand_line > .brand_detail').html(brand_detail);
        $('.content > div.content_detail > .detail_line > .product_detail').html(product_detail);
        $('.content > div.content_detail > .usage_line > .usage').html(usage);
        $('.content > div.content_detail > .in_line > .in').html(intredients);
        $('.content > div.content_detail > .review_line > .review').html(review);
    
    
        if(data.detail_col.length <= 0 && data.detail==null && data.subDetail.length <=0){
            $('.content > div.content_detail > .detail_line').css("display","none");
        }
        if(data.usage_col.length <= 0 && data.usage.length <=0 && data.subUsage.length <=0){
            $('.content > div.content_detail > .usage_line').css("display","none")
        }
        if(data.in_col.length <= 0 &&  data.intredients.length <=0){
            $('.content > div.content_detail > .in_line').css("display","none");
        }
        if(data.review_col.length <=0 ){
            $('.content > div.content_detail > .review_line ').css("display","none");
        }
}
/*
<div style = ""> <img style = "width:100%;" src="images/decoration/brand.png"/> </div>
			<div style = ""> <img style = "width:50%;height:50%;margin: 20px  0px  20px 24%;" src="images/product/BK/Acne Mask/Brands/brands.jpg"/> </div>
			<div style = ""> <img style = "width:100%;" src="images/decoration/detail.png"/> </div>
			<div style = ""> <img style = "width:100%;" src="images/decoration/usage.png"/> </div>
			<div style = ""> <img style = "width:100%;" src="images/decoration/in.png"/> </div>*/